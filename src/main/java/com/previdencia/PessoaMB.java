package com.previdencia;
import javax.faces.bean.ManagedBean;


/**
 * 
 */

/**
 * @author Eduardo
 *
 */

@ManagedBean
public class PessoaMB {

	private String nome;


	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
}
