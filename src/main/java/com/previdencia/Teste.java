/**
 * 
 */
package com.previdencia;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * @author Eduardo
 *
 */

@Entity
public class Teste {

	@Id
	private long id;
	private String nome;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	
	
}
